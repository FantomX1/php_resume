<<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="home.css" />
    <script src="main.js"></script>
</head>
<body>
<div id="home">

<div class="content">

    <nav class ="menu">
        <ul>
                <li> <a href="home.php">&emsp;Home</a></li>
                <li><a href="aboutme.php">&emsp;About Me</a></li>
                <li><a href="photos.php">&emsp;Photos</a></li>
        </ul>
        <style>
            .menu{
                width:100%;
                background: none;
                overflow: auto;
            
            }
            .menu ul{
                margin-left:0px;
                margin-top:0px;
                padding:0;
                list-style:none;
                line-height:40px;
            }
            .menu li{
                float:left;
            }
            .menu ul li a{
                margin-left: 20px;
                top:0px;
                background:rgb(255, 83, 83);
                text-decoration:none;
                width:415.2px;
                display:block;
                text-align:center;
                color:white;
                font-size:16px;
                font-family: Arial, Helvetica, sans-serif;
                letter-spacing:1;
            }
            .menu li a:hover{
            color:#fff;
            opacity:0.5;
            font-size:19px;
            }
            </style>
            </nav>
            </div>
            </div>
            <pro class="welcome">
                    <ul>
                        <center>
                                <h1>WELCOME!<br> I'm Queenie</h1>
                                <br>
                        </center>
                        <center><p><b>"Be a student</b><br>
                            as long as<br>
                            you still have something to learn<br>
                            and this will mean all<br>
                            <b>your life."</b></p></center>
                    </ul>
                </pro>
</body>
</html>