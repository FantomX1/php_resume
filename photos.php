<<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Photos</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="photos.css" />
    <script src="main.js"></script>
</head>
<body>
<div id="home">

<div class="content">

    <nav class ="menu">
        <ul>
                <li><a href="home.php">&emsp;Home</a></li>
                <li><a href="aboutme.php">&emsp;About Me</a></li>
                <li><a href="photos.php">&emsp;Photos</a></li>
        </ul>
    
        <style>
        .menu{
            width:100%;
            background: none;
            overflow: auto;
        
        }
        .menu ul{
            margin-left:0px;
            margin-top:0px;
            padding:0;
            list-style:none;
            line-height:40px;
        }
        .menu li{
            float:left;
        }
        .menu ul li a{
            margin-left: 20px;
            top:0px;
            background:rgb(255, 83, 83);
            text-decoration:none;
            width:415.2px;
            display:block;
            text-align:center;
            color:white;
            font-size:16px;
            font-family: Arial, Helvetica, sans-serif;
            letter-spacing:1;
        }
        .menu li a:hover{
            color:#fff;
            opacity:0.5;
            font-size:19px;
        }
    
        </style>
    </nav>
    </div>
    </div>
    </div>
    <br><br>
    <div class="gallery">
    <br><br><br><br><br>
                <ul>
                    <img src="1.jpg" alt="1" width="400px" height="300px">
                    <img src="2.jpg" alt="1" width="400px" height="300px">
                    <img src="3.jpg" alt="1" width="400px" height="300px">
                </ul>
            </div>
</body>
</html>